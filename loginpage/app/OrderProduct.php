<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    protected $table='order_products';  
    protected $fillable=['id','order_id','product','rate','quantity','total'];
public function order()
    {
        return $this->belongsTo('App\Product','product');
    }
    
}
