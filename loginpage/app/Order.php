<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
   protected $table='orders';  
   protected $fillable=['serial_No','name','total','date','max_no'];  
}
