<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Product;
use App\OrderProduct;

use Illuminate\Support\Facades\DB;
class OrderController extends Controller
{
   
public function index()
    {
        $orders = Order::all();   
        return view('order/index', compact('orders'));
    }
public function create(Request $request)
    {
        $data = Product::all();
        return view('order/create', compact('data')); 
    }
public function fetchRate(Request $request,$pdct)
    {
        $rate = Product::select('rate')->where('id',$pdct)->first();
        return $rate; 
    }
public function store(Request $request)  
    {  
       $request->validate([  
            'name'     =>'required',
            'date'     =>'required',
            'product'  =>'required', 
            'rate'     =>'required', 
            'quantity' =>'required', 
        ]);

        $max_no = Order::max('max_no');
       if(isset($request->product)){
       if ($max_no==null) {
         
         $max_no = 1;
        } else {
         $max_no= $max_no+1 ;
      }
      $order = Order::create([
        'max_no'    => $max_no,
        'serial_No' => 'ord/'.$max_no.'/2022',
        'name'      => $request->name,
        'date'      => $request->date,
        'total'     => $request->grandtotal,
        ]);
      }
      
        // $order = new Order; 
        // $order->serial_No = $request->get('serial_No');    
        // $order->name      = $request->get('name');
        // $order->date      = $request->get('date'); 
        // $order->total     = $request->get('grandtotal'); 
        // $order->save(); 
        
        for ($i=0;$i<count($request->product);$i++ ){
        $data = OrderProduct::create([
            'order_id' => $order->id,
            'product'  => $request->product[$i],
            'rate'     => $request->rate[$i],
            'quantity' => $request->quantity[$i],
            'total'    => $request->total[$i],
        ]);
           }
        return redirect('order');
    } 
public function edit(Request $request, $id)  
    {  
       $order    = Order::find($id);
       $data     = Product::all(); 
       $products = OrderProduct::where('order_id', $id)->get();
       return view('order/edit', compact('order','data', 'products')); 
    }  
public function update(Request $request, $id)
{
        if($request->grandtotal!=0) {
        $order = Order::find($id); 
        $order->name      = $request->get('name'); 
        $order->date      = $request->get('date');
        $order->total     = $request->get('grandtotal');  
         
        $order->save();
                
        if(isset($request->edit_delete)) {
        $orderproduct = OrderProduct::where('id',$request->edit_delete)->delete();            
        }

        if (isset($request->edit_id)) {
        foreach ($request->edit_id as $key =>$edit_id ){
        $orderproduct = OrderProduct::where('id',$edit_id)->update([
            'order_id' => $order->id,
            'product'  => $request->edit_product[$key],
            'rate'     => $request->edit_rate[$key],
            'quantity' => $request->edit_quantity[$key],
            'total'    => $request->edit_total[$key],
            ]);
            }
        }

        if (isset($request->product)) {
        foreach ($request->product as $key =>$product ){
        $orderproduct = OrderProduct::where('id',$product)->create([
            'order_id' => $order->id,
            'product'  => $request->product[$key],
            'rate'     => $request->rate[$key],
            'quantity' => $request->quantity[$key],
            'total'    => $request->total[$key],
            ]);
            }
        }
    }

        return redirect('order');  
}

public function show($id){
    $orders  = Order::find (array($id));
    $products = OrderProduct::where('order_id', $id) ->get();
    
return view('order/show', compact('orders','products'));
}
// public function show($id)  
//     {  

//        $orders  = Order::find (array($id));
//        $products = OrderProduct::join('products','products.id' , '=', 'order_products.product')
//                 ->where('order_products.order_id', $id)

//        ->selectRaw('order_products.id,products.product,order_products.rate,order_products.quantity,order_products.total')
//        ->get();
       
       
//        return view('order/show', compact('orders','products')); 
//     } 
public function destroy($id)  
    {  
       $order=Order::find($id);  
       $order->delete();
       $products = OrderProduct::where('order_id', $id);
       $products->delete();

       return redirect('order');  
    } 
     

}
