<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\Designation;

class EmployeeController extends Controller
{
    public function index()
    {
        $employees = Employee::all();   
        return view('employee/index', compact('employees'));
    }
    public function create(Request $request)
    {
        $data = Designation::all();

        return view('employee/create', compact('data')); 
    }
    public function store(Request $request)  
    {  
        
        //dd($request->all());
       $request->validate([  
            'name'                =>'required',
            'date_of_birth'       =>'required|before:20 years ago',
            'phone_no'            =>'required|unique:employees|digits:10',
            'address'             =>'required', 
            'pincode'             =>'required|digits:6',
            'designation_id'      =>'required',
            'date_of_joining'     =>'required|date_format:Y-m-d|before:today',
        ]);

        $newImageName= time() . '-' . $request->name . '-' .
        $request->image->extension();
        $request->image->move(public_path('images'), $newImageName);

      
      $employees = Employee::create([
        'name'            => $request->name,
        'date_of_birth'   => $request->date_of_birth,
        'phone_no'        => $request->phone_no,
        'address'         => $request->address,
        'pincode'         => $request->pincode,
        'designation_id'  => $request->designation_id,
        'date_of_joining' => $request->date_of_joining,
        'image_path'      => $newImageName,

        ]);

      return redirect('employee');
      }
      public function edit(Request $request, $id)  
    {  
       $data      = Designation::all(); 
       $employees = Employee::where('id', $id)->first();
       return view('employee/edit', compact('data', 'employees')); 
    }  
    public function update(Request $request, $id)
   {
     $request->validate([  
            'name'                =>'required',
            'date_of_birth'       =>'required|before:20 years ago',
            'phone_no'            =>'required|digits:10',
            'address'             =>'required', 
            'pincode'             =>'required|digits:6',
            'designation_id'      =>'required',
            'date_of_joining'     =>'required|date_format:Y-m-d|before:today',
            
        ]);

        $newImageName=null;
        if($request->image!=null){
        $newImageName= time() . '-' . $request->address . '-' .
        $request->image->extension();
        $request->image->move(public_path('images'), $newImageName);
        }
        

      $employees = Employee::find($id)->update([
        'name'            => $request->name,
        'date_of_birth'   => $request->date_of_birth,
        'phone_no'        => $request->phone_no,
        'address'         => $request->address,
        'pincode'         => $request->pincode,
        'designation_id'  => $request->designation_id,
        'date_of_joining' => $request->date_of_joining,
        'image_path'      => $newImageName,

        ]);
        
        return redirect('employee');
         }
    public function show($id){
    $employees  = Employee::find (array($id));
    return view('employee/show', compact('employees'));
    }  
    public function destroy($id)  
    {  
       $employees=Employee::find($id);  
       $employees->delete();

       return redirect('employee');  
    }  

}
