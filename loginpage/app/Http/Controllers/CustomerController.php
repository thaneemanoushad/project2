<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Customer;

class CustomerController extends Controller
{
    public function index()
    {
        $customers=Customer::all();
        return view('customer/index',compact('customers'));
    }
     public function create()
    {
        $customers=Customer::all();
        return view('customer/create',compact('customers'));
    }
     public function store(Request $request)
    {
         $request->validate([
    
    'customer.name' => 'required',
    
]);
        $customers = Customer::create([   
            'name'   => $request->name,
            ]);
        return redirect('customer');
    }
    public function edit(Request $request, $id)
    {

        $customers = Customer::find($id);
        return view('customer/edit',compact('customers'));
    }
    public function update(Request $request, $id)
    {      
       
       $customers = Customer::where('id', $id)->update([

            
            'name'   => $request->name,
            ]);
          return redirect('customer');
    }
    public function destroy($id)
    {

         $customers=Customer::find($id);  
       $customers->delete();
        return redirect('customer');

    }
}
