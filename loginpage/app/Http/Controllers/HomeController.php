<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Order;
use App\user;
use App\Role;

use Illuminate\Support\Facades\Auth;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
       // dd(Auth::user()->name);
       //dd(Auth::user()->role->id);
        if( Auth::user()->role->roll=='admin'){
        return view('home');
        }

        if( Auth::user()->role->roll!='admin'){

         $orders = Order::all();   
        return view('order/index', compact('orders'));
        }
    }
          /** 
     * Show the form for creating a new resource. 
     * 
     * @return \Illuminate\Http\Response 
    */   
   }