<?php

namespace App\Http\Controllers;
use App\User;
use App\Role;

use Illuminate\Http\Request;

class UserController extends Controller
{
   public function index()
    {
         $users = User::join('rolls','rolls.id' , '=', 'users.rollid')
                
       ->selectRaw('users.id,users.name,rolls.roll')
       ->get();
        return view('user/index', compact('users'));
    }
   public function edit(Request $request, $id)  
    {  
       $user    = User::find($id);
       $data     = Role::all(); 
       return view('user/edit', compact('user','data')); 
    }
   public function update(Request $request, $id)
    {
        $user = User::where('id', $id)->update([
            'name'   => $request->name,
            'rollid' => $request->rollid,
            ]);
            
        return redirect('user');  
    }
   public function destroy($id)  
    {  
       $user=User::find($id);  
       $user->delete();

       return redirect('user');  
    } 

}
