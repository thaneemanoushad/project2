<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
   protected $table='rolls';  
   protected $fillable=['id','name']; 
}
