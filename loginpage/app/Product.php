<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table='products';  
    protected $fillable=['product','rate'];

    public function product()
    {
        return $this->hasMany('App\OrderProduct','product');
    }
}
