<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
   protected $table='employees';  
   protected $fillable=['id','name','date_of_birth','phone_no','address','pincode','designation_id','date_of_joining','image_path'];  

   public function designation()
    {
        return $this->belongsTo('App\Designation','designation_id');
    }

}
