<?php

use Illuminate\Database\Seeder;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'y',
            'username' => 'y',
            'email' => 'y@y.com',
            'password' => \Illuminate\Support\Facades\Hash::make('vvxxyyzz'),
            'rollid' => 'super_admin',
            'is_permission' => 1,
        ]);

        User::create([
            'name' => 'admin',
            'username' => 'admin',
            'email' => 'admin@admin.com',
            'password' => \Illuminate\Support\Facades\Hash::make('adminadmin'),
            'rollid' => 'admin',
            'is_permission' => 1,
        ]);

        
    }
}

       