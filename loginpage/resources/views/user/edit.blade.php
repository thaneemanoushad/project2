@extends('layouts.app') 
@section('content')  
<head>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<form method="POST" id="edit_form"action="{{route('user.update',$user->id)}}">
    @csrf 
    @method('PUT')     
   
            
        <div class="form-group">      
              <label for="name">Name:  
              <input type="text" class="form-control" name="name" value="{{$user->name}}">
              </label><br/><br/>  
        </div>             
        <div>
          <label for="role">Roll:
              <select class="role" name="rollid" id="role" >
                    <option value=''>Select role</option>
                  @foreach ($data as $row)
                    <option value="{{ $row->id }}" >{{$row->roll }} 
                    </option>
                  @endforeach    
                    </select>
                    </label><br/><br/>
        </div>
       <button type="submit" class="btn btn-success" >save</button> 
     
</form>




@endsection 