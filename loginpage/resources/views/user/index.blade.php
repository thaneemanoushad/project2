@extends('layouts.app')  
@section('content')

<head>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<div class="container">
      <h2>user detials</h2>  
</div>

<a href="{{ url('/order') }}">Check order</a><br>


<table class="table table-bordered" >  
<thead>  
<tr>   
<th>  Name      </th> 
<th>  Roll     </th>
<th>  Delete    </th>
<th>  Edit      </th>     
</tr>  
</thead> 

 
<tbody>
    @if(isset($users))
    @foreach($users as $user)
    <tr border="none"> 
    <td>{{$user->name}}</td>   
    <td>{{($user->roll) }}</td>
    <td><form action="{{ url('/user/'.$user->id)}}" method="post">  
                  @csrf  
                  @method('DELETE')  
                  <button class="btn btn-warning" type="submit">Delete</button>
                  </form></td>
    <td>  
        <a href="{{url('/user/'.$user->id.'/edit/')}}" class="btn btn-info">Edit</a>
   </td>
    
   </tr>
   @endforeach 
   @endif
</tbody>  
</table>  
@endsection  