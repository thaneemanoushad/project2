@extends('layouts.app') 
@section('content') 
<h2>welcome here</h2>
 <div class="panel-body">
       Customer:
     <a href="{{ url('/customer/create') }}" class="btn btn-success">Create</a>
   </div> 
<table class="table table-bordered" border="1px">
	<thead>
	<tr>
	<th>Id</th>
	<th>Name</th>
	<th>Edit</th>
	<th>Delete</th>
	</tr>
	</thead>
<tbody>
	@foreach($customers as $customer)
	<tr >
        <td class="row-index text-center">
        <p>{{$customer->id}}</p></td>
		<td>{{$customer->name}}</td>
	    <td>
	      <a href="{{url('/customer/'.$customer->id.'/edit/')}}" class="btn btn-info">edit</a>

	  </td>
	  <td>
        <form action="{{ url('/customer/'.$customer->id)}}" method="post">  
                  @csrf  
                  @method('DELETE')  
                  <button class="btn btn-warning" type="submit">Delete</button>
                  </form>  
   </td>  
	    
	  
    </tr>
	@endforeach
</tbody>
</table>
@endsection 