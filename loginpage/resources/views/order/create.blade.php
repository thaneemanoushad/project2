@extends('layouts.app') 
@section('content')
<head>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>  
<form method="POST" action="{{route('order.store')}}">
    @csrf    
           
        <div class="form-group">      
              <label for="name">Name:  
              <input type="text" class="form-control" name="name">
              </label><br/><br/> 
        </div>
        <div class="form-group">      
              <label for="date">Date : 
              <input type="date" class="form-control" name="date">
              </label><br/><br/>  
        </div>               


        <button class="btn btn-md btn-primary" id="addBtn" type="button">Add new Row
        </button>
        <br/>
   <table id="mytable"> 
       <thead>
             <th>ID </th>
             <th>Product</th>
             <th>Rate</th>
             <th>Quantity</th>
             <th>Total</th>
       </thead>
           <tbody id="tbody" class="tbody">
           <tr id="R${++rowIdx}">
                <td class="row-index text-center">
                <p>1</p></td>
                <td>
                    <select class="product" name="product[]" id="product" required>
                    <option value=''>Select product</option>
                  @foreach ($data as $row)
                    <option value="{{ $row->id }}" >{{$row->product }} 
                    </option>
                  @endforeach    
                    </select>
                </td>
                <td><input class="rate" id="rate" name="rate[]" required></td>
                <td><input class="quantity" id="quantity" name="quantity[]" required></td>
                <td><input class="total" id="total" name="total[]" readonly></td>
                <td><button type="delete" class="delete" >remove</button></td>
                </tr> 
             <tfoot>
                 <tr>
                     <th id="ttl" colspan="4">Grandtotal :</th>
                     <td><input class="grandtotal" id="grandtotal" name="grandtotal" readonly></td>
                 </tr>
             </tfoot>
          </tbody>
   </table>
     <footer>
       <button type="submit" class="btn-btn" >save</button> 
     </footer> 
</form>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        var rowIdx = 1;
        $('#addBtn').on('click', function () {
            var flag =0;
            $('.product').each(function(){
            if ($(this).val()==''){
                flag=1;
             }
            });
            if(flag==0){
            $('#tbody').append(`<tr id="R${++rowIdx}">
                <td class="row-index text-center">
                <p>${rowIdx}</p></td>
                <td>
                    <select class="product" name="product[]" id="product" required>
                    <option value=''>Select product</option>
                  @foreach ($data as $row)
                    <option value="{{ $row->id }}" >{{$row->product }} 
                    </option>
                  @endforeach    
                    </select>
                </td>
                <td><input class="rate" id="rate" name="rate[]" required></td>
                <td><input class="quantity" id="quantity" name="quantity[]" required></td>
                <td><input class="total" id="total" name="total[]"readonly></td>
                <td><button type="delete" class="delete" >remove</button></td>
                </tr>`);
            }
           });
        $('#tbody').on("change","#product",function(){
            var parent = $(this).closest('tr');
            var pdct =$(parent).find('#product').val();
            $.ajax({
                type : 'get',
                url : '{{url("order/fetchRate")}}/'+pdct,
                data:{p:pdct},
                success: function(data){
                console.log(data);
                $(parent).find('#rate').val(data.rate);
                var c = calculate (parent);
                $(parent).find('#rate').val(c);
          }
      });

  }); 
        $('#tbody').on("change","#product",function(){
            var parent = $(this).closest('tr');
            var qntity = parent.find(".quantity").val("");
            var ttl    = parent.find(".total").val("");
             $(parent).$val(qntity);
             $(parent).$val(ttl); 
             });

        $('#tbody').on("keyup",".rate,.quantity",function(){
            var parent = $(this).closest('tr');
            var price =Number($(parent).find('.rate').val());
            var qntity = Number($(parent).find('.quantity').val());
            var ttl = Number(parseInt(price* qntity));
            if (price == "0" ){
                ttl=0;
                }
            if (qntity == "0" ){
                ttl=0; 
                }
            $(parent).find('.total').val(ttl); 
            grandTotal();
            });  


        function grandTotal(){
            var sum= 0;
            $('.total').each(function() {
            sum += parseInt($(this).val()|| 0);
            });
            $('#grandtotal').val(sum);
        }

        $("#mytable").on('click', '.delete', function () {
        var child = $(this).closest('tr').nextAll();
        child.each(function () {
            var id = $(this).attr('id');
            var idx = $(this).children('.row-index').children('p');
            var dig = parseInt(id.substring(1));
            idx.html(` ${dig - 1}`);
            $(this).attr('id', `R${dig - 1}`);
        });
            $(this).closest('tr').remove();
            rowIdx--;
            grandTotal();             
    });
        
});


</script>  
@endsection 