@extends('layouts.app')  
@section('content') 
<head>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<div class="container">
      <h2>ORDER</h2>  
</div>
<div class="card-body">
   <div class="panel-body">
       Order:
     <a href="{{ url('/order/create') }}" class="btn btn-success">Create</a>
   </div> 
 </div> 

<table class="table table-bordered" >  
<thead>  
<tr>  
<th>  ID        </th>  
<th>  Serial_No </th>
<th>  Name      </th> 
<th>  Date      </th>
<th>  Delete    </th>
<th>  Edit      </th>  
<th>  Show      </th>    
</tr>  
</thead> 

 
<tbody>
    @if(isset($orders))
    @foreach($orders as $order)
    <tr border="none">
    <td>{{$order->id}}</td> 
    <td>{{$order->serial_No}}</td>   
    <td>{{$order->name}}</td>
    <td>{{date('d-m-Y', strtotime($order->date)) }}</td> 
    <td>
        <form action="{{ url('/order/'.$order->id)}}" method="post">  
                  @csrf  
                  @method('DELETE')  
                  <button class="btn btn-warning" type="submit">Delete</button>
                  </form>  
   </td>  
   <td>  
        <a href="{{url('/order/'.$order->id.'/edit/')}}" class="btn btn-info">Edit</a>
   </td>  
   <td>  
        <a href="{{url('/order/'.$order->id)}}" class="btn btn-primary">Show</a>

   </td>  
   </tr>
   @endforeach 
   @endif
</tbody>  
</table>  
@endsection  
