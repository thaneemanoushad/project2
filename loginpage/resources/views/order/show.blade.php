@extends('layouts.app')  
@section('content')  
<head>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<div class="container">
      ORDER DETIALS 
</div>
@if(isset($orders)) 
    @foreach($orders as $order)
<div class="form-group">  
     <label for="serial_No">Sl.No: 
    <input type="text" class="form-control" name="name" value="{{$order->serial_No}}"readonly>
    </label><br/><br/>   

<div class="form-group">      
              <label for="name">Name:  
              <input type="text" class="form-control" name="name" value="{{$order->name}}"readonly>
              </label><br/><br/>  
        </div>
        <div class="form-group">      
              <label for="date">Date:  
              <input type="date" class="form-control" name="date" value="{{($order->date) }}"readonly>
              </label><br/><br/>  
        </div>      @endforeach
        @endif


<table id="mytable" class="table table-bordered"> 
       <thead>
             <th>ID </th>
             <th>Product</th>
             <th>Rate</th>
             <th>Quantity</th>
             <th>Total</th>
       </thead>
           <tbody id="tbody" class="tbody">
           @foreach($products as $data) 
            <tr id="R${++rowIdx}">
                <td class="row-index text-center">
                <p>{{$loop->iteration}}</p></td>
                <td>
                    {{$data->order->product}}
                   
                </td>
                <td> {{$data->rate}}</td>
                <td>{{$data->quantity}}</td>
                <td>{{$data->total}}</td>
                </tr>
                @endforeach 
             <tfoot>
                 <tr>
                     <th colspan="4">Grandtotal :</th>
                     <td>{{$orders->sum('total')}}</td>
                 </tr>
             </tfoot>
          </tbody>
   </table>    

<div class="card-body">
    <div class="panel-body">
        back to page
        <a href="{{ url('/order') }}">back</a>
    </div>
</div> 
@endsection 
                             