@extends('layouts.app') 
@section('content')  
<head>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<form method="POST" id="edit_form"action="{{route('order.update',$order->id)}}">
    @csrf 
    @method('PUT')     
   
        <div class="form-group">      
        <label for="serial_No">Sl.No:
        <input type="text" class="form-control" name="name" value="{{$order->serial_No}}"readonly>  
        </label><br/><br/> 
        </div>     
        <div class="form-group">      
              <label for="name">Name:  
              <input type="text" class="form-control" name="name" value="{{$order->name}}">
              </label><br/><br/>  
        </div>
        <div class="form-group">      
              <label for="date">Date : 
              <input type="date" class="form-control" name="date" value="{{($order->date) }}">
              </label><br/><br/>   
        </div>               


        <button class="btn btn-md btn-primary" id="addBtn" type="button">Add new Row
        </button>
        <br>
        
   <table  id="mytable"> 
       <thead>
             <th>ID </th>
             <th>Product</th>
             <th>Rate</th>
             <th>Quantity</th>
             <th>Total</th>
       </thead>
           <tbody id="tbody" class="tbody">
           @foreach($products as $product) 
            <tr id="R${++rowIdx}">
                <td class="row-index text-center">
                <p>{{$loop->iteration}}</p></td>
                <td>
                    <input  type="hidden"  name="edit_id[]" id="edit_id" value="{{$product->id}}">
                    <select class="product" name="edit_product[]" id="product"  required>
                    <option value=''>Select product</option>
                  @foreach ($data as $row)
                  <option value="{{ $row->id }}" @if (old('product') == $row->id || $row->id == $product->product)selected  @endif >{{$row->product}} 
                  @endforeach    
                    </select>
                </td>
                <td><input class="rate" id="rate" name="edit_rate[]" value="{{$product->rate}}"required></td>
                <td><input class="quantity" id="quantity" name="edit_quantity[]" value="{{$product->quantity}}"required></td>
                <td><input class="total" id="total" name="edit_total[]" value="{{$product->total}}" readonly></td>
                <td><button type="delete" name="delete[]" id="{{$product->id}}"class="delete" >remove</button></td>
                </tr>
                @endforeach 
             <tfoot>
                 <tr>
                     <th id="ttl" colspan="4">Grandtotal :</th>
                     <td><input class="grandtotal" id="grandtotal" name="grandtotal" value="{{$order->total}}" readonly ></td>
                 </tr>
             </tfoot>
          </tbody>
   </table>
     
       <button type="submit" class="btn btn-success" >save</button> 
     
</form>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        var rowIdx = $('#tbody tr').length;
        $('#addBtn').on('click', function () {
        var flag =0;
            $('.product').each(function(){

             if ($(this).val()==''){
                flag=1;
             }
            });
            if(flag==0){
            $('#tbody').append(`<tr id="R${++rowIdx}">
                <td class="row-index text-center">
                <p>${rowIdx}</p></td>
                <td>
                    <select class="product" name="product[]" id="product" required>
                    <option value=''>Select product</option>
                  @foreach ($data as $row)
                    <option value="{{ $row->id }}" >{{$row->product }} 
                    </option>
                  @endforeach    
                    </select>
                </td>
                <td><input class="rate" id="rate" name="rate[]"required></td>
                <td><input class="quantity" id="quantity" name="quantity[]"required></td>
                <td><input class="total" id="total" name="total[]"readonly></td>
                <td><button type="delete" class="delete" >remove</button></td>
                </tr>`);
            }
           });
        $('#tbody').on("change","#product",function(){
            var parent = $(this).closest('tr');
            var pdct =$(parent).find('#product').val();
            $.ajax({
                type : 'get',
                url : '{{url("order/fetchRate")}}/'+pdct,
                data:{p:pdct},
                success: function(data){
                console.log(data);
                $(parent).find('#rate').val(data.rate);
                var c = calculate (parent);
                $(parent).find('#rate').val(c);
          }
      });

  });

         $('#tbody').on("change","#product",function(){
            var parent = $(this).closest('tr');
            var qntity = parent.find(".quantity").val("");
            var ttl    = parent.find(".total").val("");
             $(parent).$val(qntity);
             $(parent).$val(ttl); 


        });
        
        

        $('#tbody').on("keyup",".rate,.quantity",function(){
            var parent = $(this).closest('tr');
            var price =Number($(parent).find('.rate').val());
            var qntity = Number($(parent).find('.quantity').val());
            var ttl = Number(parseInt(price* qntity));
            if (price == "0" ){
                ttl=0;
                }
            if (qntity == "0" ){
                ttl=0; 
                }
            $(parent).find('.total').val(ttl); 
            grandTotal();
            });  


        function grandTotal(){
            var sum= 0;
            $('.total').each(function() {
            sum += parseInt($(this).val()|| 0);
            });
            $('#grandtotal').val(sum);
        }

        $("#mytable").on('click', '.delete', function () {
        var x = confirm("are you sure");

        if(x==1){
            var z = $(this).attr('id');
            if(z!= undefined){

        $('#edit_form').append('<td><input type="hidden" id="edit_delete" name="edit_delete[]" value="'+z+'"></td>');

       }
        
        $(this).closest('tr').remove();
            grandTotal(); 
       }
       var i =1;
       $('#tbody tr').each(function(){
      $(this).closest('tr').find('td:first-child').text(i);
      i++;

       });     

                     
    });

});


</script>  
@endsection 