@extends('layouts.app')  
@section('content')  
<form method="get" action="{{ url('/order/store') }}">  
   @csrf     
          <div class="form-group">      
              <label for="serial_No">serial_No:</label><br/><br/>  
              <input type="number" class="form-control" name="serial_No"/><br/><br/>  
          </div>     
<div class="form-group">      
              <label for="name">name:</label><br/><br/>  
              <input type="text" class="form-control" name="name"/><br/><br/>  
          </div>
<div class="form-group">      
              <label for="date">date:</label><br/><br/>  
              <input type="date" class="form-control" name="date"/><br/><br/>  
          </div>               


<br/> 
 
<button class="btn btn-md btn-primary"
      id="addBtn" type="button">Add new Row
      </button>
    <br>
<table id="mytable"> 
    <thead>
        <th>ID </th>
        <th>product</th>
        <th>rate</th>
        <th>quantity</th>
        <th>total</th>
    </thead>
    <tbody id="tbody" class="tbody"> 

  <tfoot>
    <tr>
      <th id="ttl" colspan="4">Grandtotal :</th>
      <td><input class="grandtotal" id="grandtotal" name="grandtotal"></td>
    </tr>
   </tfoot>
</tbody>
</table>
<footer>
  <button type="submit" class="btn-btn" >save</button> 
</footer> 
 
</form> 

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<script>

    $(document).ready(function(){
//          $('#tbody').on("change","#product",function(){
//             var parent = $(this).closest('tr');
//             var pdct =$(parent).find('#product').val();
//             $.ajax({
//                 type : 'get',
//                 url : '{{url("order/fetchRate")}}/'+pdct,
//                 data:{p:pdct},
//                 success: function(data){
//                 console.log(data);
//                 $(parent).find('#rate').val(data.rate);
//                 var c = calculate (parent);
//                 $(parent).find('#rate').val(c);
//         }
//     });
// });

var rowIdx = 0;

$('#addBtn').on('click', function () {
  
    $('#tbody').append(`<tr id="R${++rowIdx}">
          <td class="row-index text-center">
                <p>${rowIdx}</p></td>
     <td>
       <select class="form-control" name="product[]" id="product">
    <option>Select product</option>
    @foreach ($data as $row)
        <option value="{{ $row->id }}" >{{$row->product }} 
        </option>
    @endforeach    
    </select>
    </td>
      <td><input class="rate" id="rate" name="rate[]"></td>
      <td><input class="quantity" id="quantity" name="quantity[]"></td>
      <td><input class="total" id="total" name="total[]"></td>
      <td><button type="delete" class="delete" >remove</button></td>
           </tr>`);
});
     
  
     $("#mytable").on('click', '.delete', function () {
        var child = $(this).closest('tr').nextAll();
        child.each(function () {
            var id = $(this).attr('id');
            var idx = $(this).children('.row-index').children('p');
            var dig = parseInt(id.substring(1));
            idx.html(` ${dig - 1}`);
            $(this).attr('id', `R${dig - 1}`);
        });
        $(this).closest('tr').remove();
         rowIdx--;
      
          grandTotal();             
});
     $('#tbody').on("keyup",".rate,.quantity",function(){
       
            var parent = $(this).closest('tr');
            var price =Number($(parent).find('.rate').val());
            var qntity = Number($(parent).find('.quantity').val());
            var ttl = Number(parseInt(price* qntity));
              if (price == "0" ){
               ttl=0; 
              }
              if (qntity == "0" ){
               ttl=0; 
              }
              
              


            $(parent).find('.total').val(ttl);
             grandTotal();

        });
   



    function grandTotal(){
        var sum= 0;

        $('.total').each(function() {
          sum += parseInt($(this).val());
});

        $('#grandtotal').val(sum);
       
       
    }
      });
                
</script> 





@endsection 


