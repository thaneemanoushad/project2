@extends('layouts.app')  
@section('content') 
<div class="card-body">
 <div class="panel-body">
    Create order:
    <a href="{{ url('/order/create') }}">create</a>
 </div> 
 </div> 
<table border="1px">  
<thead>  
<tr>  
<td>  ID </td>  
<td>  serial_No </td>
<td>  name </td> 
<td>  date </td>
<td>  delete </td>
<td>  edit </td>  
<td>  show </td>    
</tr>  
</thead>  
<tbody>  
    @if(isset($cruds))
@foreach($cruds as $crud)  
        <tr border="none">  
            <td>{{$crud->id}}</td>  
            <td>{{$crud->serial_No}}</td>   
            <td>{{$crud->name}}</td>
            <td>{{date('d-m-Y', strtotime($crud->date)) }}</td>    
<td >  
<form action="{{ url('/order/destroy/'.$crud->id)}}" method="get">  
                  @csrf  
                  @method('DELETE')  
                  <button class="btn btn-danger" type="submit">Delete</button>  
                </form>  
</td>  
<td >  
<form action="{{url('/order/edit/'.$crud->id)}}" method="GET">  
                  @csrf  
                   
                  <button class="btn btn-danger" type="submit">Edit</button>  
                </form>  
</td>  
<td >  
<form action="{{ url('/order/show/'.$crud->id)}}" method="GET">  
                  @csrf  
                   
                  <button class="btn btn-danger" type="submit">Show</button>  
                </form>  
</td>  
  
         </tr>  
@endforeach  
@endif
</tbody>  
</table>  
@endsection  
