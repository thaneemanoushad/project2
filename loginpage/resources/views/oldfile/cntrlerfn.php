 
use Illuminate\Http\Request;
use App\Crud;
use App\Product;
use App\Order;



 public function page()  
    {  
         $cruds = Crud::all();  
  
        return view('order/index', compact('cruds'));   
    }    
  
    /** 
     * Show the form for creating a new resource. 
     * 
     * @return \Illuminate\Http\Response 
     */  
    public function create()  
    {  
     $cruds = Crud::all(); 
   return view('order/create', compact('cruds'));   
    }  
  
    /** 
     * Store a newly created resource in storage. 
     * 
     * @param  \Illuminate\Http\Request  $request 
     * @return \Illuminate\Http\Response 
     */  
    public function store(Request $request)  
    {  
       
        $request->validate([  
            'serial_No'=>'required',
            'name'=>'required',
            'date'=>'required',
            'order_id'=>'required', 
            'product'=>'required', 
            'rate'=>'required', 
            'quantity'=>'required', 

         
              
        ]);  
  
        $crud = new Crud; 
        $crud->serial_No =  $request->get('serial_No');    
        $crud->name = $request->get('name');
        $crud->date = $request->get('date');  
        $crud->save(); 
        
        
        for ($i=0;$i<count($request->product);$i++ ){

        $data = Order::create([
   
          'order_id' => $crud->id,
          'product' =>  $request->product[$i],
          'rate' => $request->rate[$i],
          'quantity' => $request->quantity[$i],
          'total' =>  $request->total[$i],
          
      ]);
        
    }

   

          return redirect('order/index');
    }  
  
    /** 
     * Display the specified resource. 
     * 
     * @param  int  $id 
     * @return \Illuminate\Http\Response 
     */  
    public function edit($id)  
    {  
        $crud= Crud::find($id); 
     return view('order/edit', compact('crud')); 
    }  
  
    /** 
     * Update the specified resource in storage. 
     * 
     * @param  \Illuminate\Http\Request  $request 
     * @param  int  $id 
     * @return \Illuminate\Http\Response 
     */  
    public function update(Request $request, $id)  
    {  
         $request->validate([  
            'serial_No'=>'required',    
            'name'=>'required',
            'date'=>'required',
        ]);  
         
        $crud = Crud::find($id); 
        $crud->serial_No =  $request->get('serial_No');   
        $crud->name = $request->get('name'); 
        $crud->date = $request->get('date');  
         
        $crud->save(); 

        return redirect('order/index');  
    }  

    /** 
     * Remove the specified resource from storage. 
     * 
     * @param  int  $id 
     * @return \Illuminate\Http\Response 
     */  
      public function show($id)  
    {  
        $crud= Crud::find($id);  
     return view('order/show', compact('crud')); 
    }  
  
    /** 
     * Show the form for editing the specified resource. 
     * 
     * @param  int  $id 
     * @return \Illuminate\Http\Response 
     */  
    public function destroy($id)  
    {  
       $crud=Crud::find($id);  
       $crud->delete();
    return redirect('order/index');  
    }  

    public function dropDownShow(Request $request)

   {
      $data = Product::all();

     return view ('order/create',['data'=>$data]);
    } 
    
     public function fetchRate(Request $request,$pdct)

   {
      $rate = Product::select('rate')->where('id',$pdct)->first();

     return $rate;
    } 
    
    }