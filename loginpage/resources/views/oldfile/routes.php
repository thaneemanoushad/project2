Route::get('/order/index', 'HomeController@page');
Route::get('/order/create', 'HomeController@create');
Route::get('/order/store', 'HomeController@store');
Route::get('/order/destroy/{id}', 'HomeController@destroy');
Route::get('/order/edit/{id}', 'HomeController@edit');
Route::get('/order/update/{id}', 'HomeController@update');
Route::get('/order/show/{id}', 'HomeController@show');
Route::get('/order/create', 'HomeController@dropDownShow');
Route::get('/order/create/{pdct}', 'HomeController@fetchRate');
Route::get('/order/edit', 'HomeController@dropDown');
//Route::get('/order/update/{id}', 'HomeController@updateTable');


