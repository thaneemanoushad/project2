@extends('layouts.app') 
@section('content')

<form method="POST" action="{{route('employee.store')}}" enctype="multipart/form-data">
    @csrf    
          
        <div class="form-group">      
              <label for="name">Name:  
              <input type="text" class="form-control" name="name" placeholder="name" required>
              </label><br/><br/> 
        </div>
        <div class="form-group">      
              <label for="date">Date of birth  : 
              <input type="date" class="form-control" name="date_of_birth" required>
              </label><br/><br/>  
        </div>   
        <div class="form-group">      
              <label for="phone_no">Phone no :  
              <input type="text" class="form-control" name="phone_no" placeholder="phone no" required>
              </label><br/><br/> 
        </div>
        <div class="form-group">      
              <label for="address">Address  :  
              <textarea class="form-control" style="height:150px" name="address" placeholder="address" required></textarea>
              </label><br/><br/> 
        </div>
        <div class="form-group">      
              <label for="pincode">Pincode  :  
              <input type="text" class="form-control" name="pincode" placeholder="pincode" required>
              </label><br/><br/> 
        </div>
        <div>
          <label for="designation_id">Designation:
              <select class="designation_id" name="designation_id" id="designation_id" required>
                    <option value=''>Select designation</option>
                  @foreach ($data as $row)
                    <option value="{{ $row->id }}" >{{$row->designation }} 
                    </option>
                  @endforeach    
                    </select>
                    </label><br/><br/>
        </div>
        <div class="form-group">      
              <label for="date">Date of joining  : 
              <input type="date" class="form-control" name="date_of_joining" required>
              </label><br/><br/>  
        </div>
        <div class="form-group">
                <strong>Image:</strong>
                <input type="file" name="image" class="form-control" >
        </div><br/><br/>             
     <footer>
       <button type="submit" class="btn-btn" >save</button> 
     </footer> 
</form>



@endsection 