<form method="POST" id="edit_form"action="{{route('employee.update',$employees->id)}}" enctype="multipart/form-data">
    @csrf 
    @method('PUT')  

       <div class="form-group">      
              <label for="name">Name:  
              <input type="text" class="form-control" name="name"  value="{{$employees->name}}"required>
              </label><br/><br/> 
        </div>
        <div class="form-group">      
              <label for="date">Date of birth  : 
              <input type="date" class="form-control" name="date_of_birth" value="{{$employees->date_of_birth}}" required>
              </label><br/><br/>  
        </div>   
        <div class="form-group">      
              <label for="phone_no">Phone no :  
              <input type="text" class="form-control" name="phone_no"  value="{{$employees->phone_no}}" required>
              </label><br/><br/> 
        </div>
        <div class="form-group">      
              <label for="address">Address  :  
              <input type="text"class="form-control"  name="address"  value="{{$employees->address}}" required>
              </label><br/><br/> 
        </div>
        <div class="form-group">      
              <label for="pincode">Pincode  :  
              <input type="text" class="form-control" name="pincode"  value="{{$employees->pincode}}" required>
              </label><br/><br/> 
        </div>
        <div>
          <label for="designation_id">Designation:
              <select class="designation_id" name="designation_id" id="designation_id" required>
                    <option value=''>Select designation</option>
                  @foreach ($data as $row)
                    <option value="{{ $row->id }}"  @if (old('designation_id') == $row->id || $row->id == $employees->designation_id)selected  @endif  >{{$row->designation }} 
                    </option>
                  @endforeach    
                    </select>
                    </label><br/><br/>
        </div>
        <div class="form-group">      
              <label for="date">Date of joining  : 
              <input type="date" class="form-control" name="date_of_joining"  value="{{$employees->date_of_joining}}"required>
              </label><br/><br/>  
        </div>
       <p>
                <input type="file" name="image" onchange="previewFile(this);">
            </p>
            <div class="stn_uploader">
            <button type="button" class="delete">Remove</button><br>
            <div></br>
            <img id="previewImg" src="{{ URL::asset('images/'.$employees->image_path )}}" alt="Placeholder">
            </div>
            </div><br>
                    
       <button type="submit" class="btn btn-success" >save</button> 
     
</form>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<script>

    function previewFile(input){
            var file = $("input[type=file]").get(0).files[0];
            if(file){
                var reader = new FileReader();
                reader.onload = function(){
                    $("#previewImg").attr("src", reader.result);
                }
     
                reader.readAsDataURL(file);
            }
        }


     $(document).ready(function() {
    $('.stn_uploader .delete').click(function() {
          $(this).closest('.stn_uploader').find('img').remove();  
    });
});


  
</script>
  

