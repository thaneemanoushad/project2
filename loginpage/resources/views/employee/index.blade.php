@extends('layouts.app')  
@section('content') 
<head>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<div class="container">
      <h2>EMPLOYEE</h2>  
</div>
<div class="card-body">
   <div class="panel-body">
       Employee:
     <a href="{{ url('/employee/create') }}" class="btn btn-success">Create</a>
   </div> 
 </div> 
<table class="table table-bordered" >  
<thead>  
<tr>  
<th>  ID                 </th>  
<th>  Name               </th> 
<th>  Date of birth      </th> 
<th>  Phone no           </th>
<th>  Address            </th> 
<th>  Pincode            </th> 
<th>  Designation        </th> 
<th>  Date of joining    </th> 
<th>  Delete             </th>
<th>  Edit               </th>  
<th>  Show               </th>    
</tr>  
</thead> 

 
<tbody>
    @if(isset($employees))
    @foreach($employees as $employee)
    <tr border="none">
    <td>{{$employee->id}}</td> 
    <td>{{$employee->name}}</td>
    <td>{{date('d-m-Y', strtotime($employee->date_of_birth)) }}</td> 
    <td>{{$employee->phone_no}}</td>
    <td>{{$employee->address}}</td>
    <td>{{$employee->pincode}}</td>
    <td>{{$employee->designation->designation}}</td>
    <td>{{date('d-m-Y', strtotime($employee->date_of_joining)) }}</td> 
    <td>
        <form action="{{ url('/employee/'.$employee->id)}}" method="post">  
                  @csrf  
                  @method('DELETE')  
                  <button class="btn btn-warning" type="submit">Delete</button>
                  </form>  
   </td>  
   <td>  
        <a href="{{url('/employee/'.$employee->id.'/edit/')}}" class="btn btn-info">Edit</a>
   </td>  
   <td>  
        <a href="{{url('/employee/'.$employee->id)}}" class="btn btn-primary">Show</a>

   </td>  
   </tr>
   @endforeach 
   @endif
</tbody>  
</table>  
@endsection  
