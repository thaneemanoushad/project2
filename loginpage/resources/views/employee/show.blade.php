<div class="container">
      EMPLOYEE DETIALS 
</div><br>
@if(isset($employees)) 
    @foreach($employees as $employee)
    <div class="form-group">      
              <label for="name">Name: {{$employee->name}}
              </label><br/><br/> 
        </div>
        <div class="form-group">      
              <label for="date">Date of birth  : {{date('d-m-Y', strtotime($employee->date_of_birth)) }}
              </label><br/><br/>  
        </div>   
        <div class="form-group">      
              <label for="phone_no">Phone no : {{$employee->phone_no}} 
              </label><br/><br/> 
        </div>
        <div class="form-group">      
              <label for="address">Address  :  {{$employee->address}}
              </label><br/><br/> 
        </div>
        <div class="form-group">      
              <label for="pincode">Pincode  :  {{$employee->pincode}}
              </label><br/><br/> 
        </div>
        <div>
          <label for="designation_id">Designation:{{$employee->designation->designation}}
              </label><br/><br/>
        </div>
        <div class="form-group">      
              <label for="date">Date of joining  : {{date('d-m-Y', strtotime($employee->date_of_joining)) }}
              </label><br/><br/>  
        </div>
        @if($employee->image_path!=null)
        <div class="form-group">
        <img src="{{ URL::asset('images/'.$employee->image_path )}}" >
         image 
   @endif 
    </div><br/><br/> 
        @endforeach
        @endif
        <div class="card-body">
        <div class="panel-body">
        back to page
        <a href="{{ url('/employee') }}">back</a>
       </div>
       </div> 

  