<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\superUserController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('home', [HomeController::class, 'index'])->name('home'); 
Route::group(['middleware' => ['super_admin']], function () {
Route::get('super_admin-view', [HomeController::class, 'super_adminView'])->name('super_admin.view');
}); 


Route::resource('order', OrderController::class);
Route::get('/order/fetchRate/{p}', 'OrderController@fetchRate');

Route::resource('user',UserController::class);
Route::resource('customer',CustomerController::class);
Route::resource('employee',EmployeeController::class);

//Route::get('/order/index', 'OrderController@index');
// Route::get('/order/create', 'OrderController@create');
// Route::get('/order/store', 'OrderController@store');
// Route::get('/order/show/{id}', 'OrderController@show');
// Route::get('/order/edit/{id}', 'OrderController@edit');
// Route::get('/order/update/{id}', 'OrderController@update');
// Route::get('/order/destroy/{id}','OrderController@destroy');









